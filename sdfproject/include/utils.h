#ifndef  _UTILS_H_
#define  _UTILS_H_

int Hex2Char(int fromi,char *toc);
int Char2Hex(char fromc,int *toi);
int INT2ByteArr(int i,char *ba);
int ByteArr2INT(char *ba,int *i);

#endif
