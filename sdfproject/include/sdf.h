#ifndef __SDF_H
#define __SDF_H
//定义设备信息结构
typedef struct DeviceInfo_st{
	unsigned char IssuerName[40]; //设备生产厂商名称
	unsigned char DeviceName[16]; 
	unsigned char DeviceSerial[16]; 
	unsigned int DeviceVersion; 
	unsigned int StandardVersion; 
	unsigned int AsymAlgAbility[2]; 
	unsigned int SymAlgAbilty; 
	unsigned int HashAlgAbility; 
	unsigned int BufferSize;
}DEVICEINFO;

// Error Code
#define SDR_OK 0x0   //操作成功

//********************************
//设备管理
//********************************

/*
功能：打开密码设备。
参数∶
phDeviceHandle[out] 返回设备句柄

返回值∶
   0   成功
  非0  失败，返回错误代码
*/
int SDF_OpenDevice(void ** phDeviceHandle);

/*
功能∶关闭密码设备，并释放相关资源。
参数∶
hDeviceHandle[in] 已打开的设备句柄
返回值∶ 
	0（SDR_OK）	成功
	非0	失败，返回错误代码
*/
int SDF_CloseDevice(void *hDeviceHandle);

/*


功能∶获取密码设备能力描述。;
参数∶
hSesionHandle[in]与设备建立的会话句柄 
pstDevceInfo [out]设备能力描述信息，内容及格式见设备信息定义
返回值∶ 
	0（SDR_OK）	成功
	非0	失败，返回错误代码
*/
int SDF_GetDeviceInfo( void * hSessionHandle, 
                       DEVICEINFO * pstDeviceInfo);



/*
功能：获取指定长度的随机数
参数：
 uiLength[in]  欲获取的随机数长度 
 pucRandom[ out] 缓冲区指针，用于存放获取的随机数
 返回值∶ 
	 00（SDR_OK）	成功
	非0	失败，返回错误代码
*/
int SDF_GenerateRandom (void * hSessionHandle, unsigned int uiLength, unsigned char * pucRandom);

#endif
