#include "sdf.h"
#include <string.h>
#include <time.h>
#include <stdlib.h>

//********************************
//设备管理
//********************************

int SDF_OpenDevice(void ** phDeviceHandle){
	return SDR_OK;
}


int SDF_CloseDevice(void *hDeviceHandle){
	
	return SDR_OK;
}

int SDF_GetDeviceInfo( void * hSessionHandle, DEVICEINFO * pstDeviceInfo) {
    
    DEVICEINFO di;
	strcpy(di.IssuerName,"RocSDF");
	strcpy(di.DeviceName,"SDFBESTI181x");
	strcpy(di.DeviceSerial,"2021040001");
	di.DeviceVersion = 1;
    //...

    //pstDevicelnfo = &di;
    *pstDeviceInfo = di;

	return SDR_OK;
}

static int myRandom(){
    srand((unsigned)time(NULL));
    return rand();
}

int SDF_GenerateRandom (void * hSessionHandle, unsigned int uiLength, unsigned char * pucRandom){

	
    for(int i=0; i<uiLength; i++){
        sleep(2); 
        *(pucRandom+i) = myRandom();
       // pucRandom[i] = i;
    }

	return SDR_OK;
}


