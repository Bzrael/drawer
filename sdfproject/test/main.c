#include "sdf.h"
#include <stdio.h>
#include <stdlib.h>

int main(){

    void ** pdh;
	pdh = (void **) malloc(20);
	int ret;

    ret = SDF_OpenDevice(pdh);
	if(ret != SDR_OK){
        printf("error!");
	} else {
		printf("device opened!\n");
	}

    DEVICEINFO testdi;
    ret = SDF_GetDeviceInfo(pdh, &testdi);
	if(ret != SDR_OK){
        printf("error!");
	} else {
        printf("Issuer Name: %s\n", testdi.IssuerName);
        printf("Device Name: %s\n", testdi.DeviceName);
        printf("Device Serial: %s\n", testdi.DeviceSerial);
        printf("Device Version: %d\n", testdi.DeviceVersion);
		
	}

    char pRandom[10];

    ret = SDF_GenerateRandom(*pdh,10, pRandom);
	if(ret != SDR_OK){
        printf("error!");
	} else {
        for(int i=0; i<10; i++)
            printf("%d\n", pRandom[i]);
	}


    ret = SDF_CloseDevice(*pdh);
	
	if(ret != SDR_OK){
        printf("error!");
	} else {
		free(pdh);
		printf("device closed!\n");
	}

	return 0;
}
