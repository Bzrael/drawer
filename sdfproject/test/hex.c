#include<stdio.h>
#include"utils.h"
int main(){
   //把整型数字转化为字节数组
  int p;
  char bytearr[100];
  printf("请输入一个整型数字\n");
  scanf("%d",&p);
  printf("把整型数转化为字节数组\n");
  INT2ByteArr(p,bytearr);
  printf("字节数组为%s\n",bytearr);
 
  //把字节数组转化为整型数字
  char barr[100];
  int h;
  printf("请输入一个字节数组:");
  scanf("%s",barr);
  printf("把字节数组转化为整型数字:");
  ByteArr2INT(barr,&h);
  printf("%d\n",h);
}